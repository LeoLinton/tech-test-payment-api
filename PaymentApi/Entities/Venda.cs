using PaymentApi.Enums;

namespace PaymentApi.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusVenda Status { get; set; }
        public Vendedor? Vendedor { get; set; }
        public List<Item>? Itens { get; set; }
    }
}