using Microsoft.EntityFrameworkCore;
using PaymentApi.Entities;

namespace PaymentApi.Context
{
    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options)
        {

        }

        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Venda> Vendas { get; set; }

    }
}