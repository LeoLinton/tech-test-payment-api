using Microsoft.AspNetCore.Mvc;
using PaymentApi.Entities;
using PaymentApi.Context;

namespace PaymentApi.Controllers
{
    public class VendedorController : ControllerBase
    {
        private readonly PaymentContext _context;
        public VendedorController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("CadastrarVendedor")]
        public IActionResult CadastrarVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();

            return Ok(vendedor);
        }

        [HttpGet("BuscarVendedorPorId")]
        public IActionResult BuscarVendedorPorId(int id)
        {
            var vendedor = _context.Vendedores.Find(id);

            if (vendedor == null)
            {
                return NotFound();
            }

            return Ok(vendedor);
        }

        [HttpGet("BuscarTodosVendedores")]
        public IActionResult BuscarTodosVendedores()
        {
            var vendedor = _context.Vendedores;
            return Ok(vendedor);
        }

        [HttpPut("AtualizarVendedor")]
        public IActionResult Atualizar(int id, Vendedor vendedor)
        {
            var vendedorBd = _context.Vendedores.Find(id);

            if (vendedorBd == null)
            {
                return NotFound();
            }

            vendedorBd.Nome = vendedor.Nome;
            vendedorBd.Cpf = vendedor.Cpf;
            vendedorBd.Email = vendedor.Email;
            vendedorBd.Telefone = vendedor.Telefone;

            _context.Vendedores.Update(vendedorBd);
            _context.SaveChanges();
            return Ok(vendedorBd);
        }

        [HttpDelete("DeletarVendedor")]
        public IActionResult Deletar(int id)
        {
            var vendedor = _context.Vendedores.Find(id);

            if (vendedor == null)
            {
                return NotFound();
            }
            _context.Vendedores.Remove(vendedor);
            _context.SaveChanges();
            return NoContent();
        }
    }
}