using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PaymentApi.Context;
using PaymentApi.Entities;
using PaymentApi.Enums;
using System.ComponentModel.DataAnnotations;

namespace PaymentApi.Controllers
{
    public class VendaController : ControllerBase
    {
        private readonly PaymentContext _context;

        public VendaController(PaymentContext context)
        {
            _context = context;
        }

        [HttpGet("BuscarVendaPorId")]
        public IActionResult BuscarVenda(int id)
        {
            var vendaTest = _context.Vendas.Find(id);

            if (vendaTest is null)
                return NotFound();

            var venda = _context.Vendas.Where(x => x.Id == id)
                .Include(v => v.Vendedor).Include(i => i.Itens);

            return Ok(venda);
        }

        [HttpPost("RegistrarVenda")]
        public IActionResult RegistrarVenda([Required] int IdVendedor, [Required] int IdItem)
        {
            Venda venda = new Venda();
            List<Item> items = new List<Item>();
            var vendedor = _context.Vendedores.Find(IdVendedor);
            var item = _context.Items.Find(IdItem);

            items.Add(item);

            if (vendedor is null)
                return NotFound("Vendedor não encontrado!!!");

            if ((items.Count() == null) || (items.Count() == 0))
                return BadRequest("Venda deve possuir pelo menos 1 item!");

            venda.Status = EnumStatusVenda.AguardandoPagamento;
            venda.Vendedor = vendedor;
            venda.Itens = items;
            venda.Data = DateTime.Now;

            _context.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(BuscarVenda), new { id = venda.Id }, venda);
        }

        [HttpPut("AtualizarVendaPorId")]
        public IActionResult AtualizarVendaPorId(int id, EnumStatusVenda status)
        {
            var venda = _context.Vendas.Find(id);

            if (venda is null)
                return NotFound();

            if (Status(venda.Status, status))
            {
                venda.Status = status;

                _context.Vendas.Update(venda);
                _context.SaveChanges();

                return Ok(venda);
            }
            else
            {
                return BadRequest("Transição de Status Inválida!");
            }
        }

        private bool Status(EnumStatusVenda StatusOld, EnumStatusVenda StatusNew)
        {

            if (StatusOld == EnumStatusVenda.AguardandoPagamento
                && (StatusNew == EnumStatusVenda.PagamentoAprovado || StatusNew == EnumStatusVenda.Cancelado))
            {
                return true;
            }

            if (StatusOld == EnumStatusVenda.PagamentoAprovado &&
               (StatusNew == EnumStatusVenda.EnviadoParaTransportadora || StatusNew == EnumStatusVenda.Cancelado))
            {
                return true;
            }

            if (StatusOld == EnumStatusVenda.EnviadoParaTransportadora &&
               StatusNew == EnumStatusVenda.Entregue)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

    }
}