using Microsoft.AspNetCore.Mvc;
using PaymentApi.Entities;
using PaymentApi.Context;

namespace PaymentApi.Controllers
{
    public class ItemController : ControllerBase
    {
        private readonly PaymentContext _context;

        public ItemController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("CadastrarItem")]
        public IActionResult CadastrarItem(Item item)
        {
            _context.Add(item);
            _context.SaveChanges();

            return Ok(item);
        }

        [HttpGet("BuscarItemPorId")]
        public IActionResult BuscarItemPorId(int id)
        {
            var item = _context.Items.Find(id);

            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        [HttpGet("BuscarTodosItens")]
        public IActionResult BuscarTodosItens()
        {
            var item = _context.Items;
            return Ok(item);
        }

        [HttpDelete("DeletarItem")]
        public IActionResult DeletarItem(int id)
        {
            var item = _context.Items.Find(id);

            if (item == null)
            {
                return NotFound();
            }
            _context.Items.Remove(item);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("AtualizarItem")]
        public IActionResult AtualizarItem(int id, Item item)
        {
            var itemUpdate = _context.Items.Find(id);

            if (itemUpdate == null)
            {
                return NotFound();
            }

            itemUpdate.Nome = item.Nome;
            itemUpdate.Preco = item.Preco;

            _context.Items.Update(itemUpdate);
            _context.SaveChanges();
            return Ok(itemUpdate);
        }
    }
}